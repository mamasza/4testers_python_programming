first_name = "Magda"
last_name = "Cieśla"
age = 49

print(first_name)
print(last_name)
print(age)

simple_equation_result = 2 * 5 + 3
print(simple_equation_result)
name = "Tosia"
coat_color = 'white'
age_in_months = 3 * 12 + 4
weight = 2.1
number_of_paws = 4
is_male = False

print(name, age_in_months, weight, number_of_paws, coat_color, is_male)

# Below I describe my friendship details as a set of variables
name_of_friend = "Anna"
age_of_friend = 49
number_of_animals = 3
has_driving_license = True
period_of_friendship_in_years = 35.544
print(name_of_friend, age_of_friend, number_of_animals, has_driving_license, period_of_friendship_in_years, sep = ' <***> ')
my_bank_total_account = 6_000_000
my_friends_account = 5e12
print(my_friends_account,my_bank_total_account)

def upper_word(word):
    return word.upper()

print(upper_word("kasia"))

